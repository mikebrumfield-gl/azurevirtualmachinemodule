# Terraform Example Defintion

In this project we define a base template of Azure-based infrastructure as a Terraform file. Azure users who wish to manage infrastructure using these templates as a base can do so with Terragrunt


## Resources Created
- Resource Group
- Virtual Machine
- Vnet
  - Subnet 
  - security group
- Storage Account
- SSH Key

## Commands
- Create
    - `terraform init -backend-config=backend.conf`
    - `terraform plan -var-file="input.tfvars" -out main.tfplan`
    - `terraform apply main.tfplan`
- Destroy
    - `terraform plan -destroy -out main.destroy.tfplan`
    - `terraform apply main.destroy.tfplan`