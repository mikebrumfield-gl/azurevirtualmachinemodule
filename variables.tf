variable "resource_group_name_prefix" {
  description   = "Prefix of the resource group name that's combined with a random ID so name is unique in your Azure subscription."
}

variable "vm_name" {
  description   = "Name of the virtual machine to create"
}

variable "username" {
  description   = "Username of the initial user to create for the VM. This user will have root access to the node"
}

variable "vm_size" {
  description   = "Instance size of the VM"
  default = "Standard_B2s"
}

variable "resource_group_location" {
  default       = "centralus"
  description   = "Location of the resource group."
}